class AddCardIdUserIdToAnswers < ActiveRecord::Migration
  def change
    add_column :answers, :card_id, :integer
    add_column :answers, :user_id, :integer
  end
end
