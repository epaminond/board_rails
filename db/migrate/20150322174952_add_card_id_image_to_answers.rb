class AddCardIdImageToAnswers < ActiveRecord::Migration
  def change
    def self.up
      add_attachment :answers, :image
      add_column :answers, :card_id, :integer
      add_column :answers, :user_id, :integer
    end

    def self.down
      remove_attachment :answers, :image
      remove_column :answers, :card_id
      remove_column :answers, :user_id
    end
  end
end
