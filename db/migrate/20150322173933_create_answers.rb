class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.string :variant_ids, array: true, default: []
      t.text :text

      t.timestamps null: false
    end
  end
end
