class AddCardIdToVariants < ActiveRecord::Migration
  def change
    add_column :variants, :card_id, :integer, null: false
  end
end
