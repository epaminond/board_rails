class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|

      t.timestamps null: false
      t.string :card_type, null: false
      t.string :text, null: false
      t.integer :points, null: false
      t.boolean :favourite, null: false, default: false
    end
  end
end
