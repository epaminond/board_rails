class MakeVariantsCardIdNullable < ActiveRecord::Migration
  def change
    change_column :variants, :card_id, :integer, null: true
  end
end
