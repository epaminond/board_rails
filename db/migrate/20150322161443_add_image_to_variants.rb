class AddImageToVariants < ActiveRecord::Migration
  def self.up
    add_attachment :variants, :image
  end

  def self.down
    remove_attachment :variants, :image
  end
end
