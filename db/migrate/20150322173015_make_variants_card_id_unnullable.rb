class MakeVariantsCardIdUnnullable < ActiveRecord::Migration
  def change
    change_column :variants, :card_id, :integer, null: false
  end
end
