class CardsController < ApplicationController
  def index
    render json: {data: Card.all.as_json(include: {variants: {methods: 'image_url'}})}
  rescue e
    render json: {error: e}
  end
end
