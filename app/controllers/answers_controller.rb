class AnswersController < ApplicationController
  def create
    answer = Answer.create answers_params
    render json: {data: answer}
  rescue e
    render json: {error: e}
  end

  private

  def answers_params
    params.require(:answer).permit(:text, :image, :variant_ids, :card_id, :user_id)
  end
end
