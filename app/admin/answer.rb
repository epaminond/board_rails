ActiveAdmin.register Answer do

  form html: { enctype: "multipart/form-data" } do |f|
    f.inputs do
      f.input :text
      f.input :card
      f.input :user
      f.input :image, as: :file, hint: f.template.image_tag(f.object.image.url(:normalized))
    end

    f.actions
  end

  permit_params :text, :image, :card_id, :user_id

end
