class Variant < ActiveRecord::Base
  belongs_to :card
  has_attached_file :image, styles: { normalized: "300x300#" }, default_style: :normalized, default_url: ''
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  def image_url
    image.url
  end
end
