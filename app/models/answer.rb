class Answer < ActiveRecord::Base
  has_attached_file :image, styles: { thumb: "300x300#" }, default_style: :thumb
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  belongs_to :card
  belongs_to :user
end
