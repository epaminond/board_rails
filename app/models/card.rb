class Card < ActiveRecord::Base
  has_many :variants
  has_many :answers
  validates :card_type, inclusion: { in: %w(text select multiple_select image_upload),
    message: "%{value} is not a valid card_type" }

  def to_s
    text
  end
end
