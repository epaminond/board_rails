# Board project

This is a Rails-based backend for a board app. An app has cards with different types of questions and can store user's answers to them.

You can also check app [mockup](http://lapioworks.github.io/board-proto.framer/) and [repo of android app](https://github.com/AlexanderMyznikov/cards/).

## Development steps

To run an app you need to:

1. Clone repository

2. Setup DB. App uses PostgresSQL as database, so you need to have it installed. To configure database connection settings, please edit `database.yml` (which is added to `.gitignore` file) under config directory (you can copy `database.yml.example` and edit it afterwards). You should then be able to create a database: `rake db:reset`

3. Run app via `rails s` command